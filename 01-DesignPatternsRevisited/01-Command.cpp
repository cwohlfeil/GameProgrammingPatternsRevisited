/* Commands are an object-oriented replacement for callbacks, a method call wrapped in an object.
A simple example is taking input and translating it to actions.

void InputHandler::handleInput() {
  if (isPressed(BUTTON_X)) jump();
  else if (isPressed(BUTTON_Y)) fireGun();
  else if (isPressed(BUTTON_A)) swapWeapon();
  else if (isPressed(BUTTON_B)) lurchIneffectively();
}

This is hard coded, so not very useful. Instead, we can use the design pattern for some abstraction and reusability:
*/

class Command {
    // Base class for commands
    public:
        virtual ~Command() {}
        virtual void execute() = 0;
};

// Concrete commands
class JumpCommand : public Command {
    public:
        virtual void execute() { jump(); }
};

class FireCommand : public Command {
    public:
        virtual void execute() { fireGun(); }
};

class InputHandler {
    public:
        void handleInput();

        // Methods to bind commands...

    private:
        Command* buttonX_;
        Command* buttonY_;
        Command* buttonA_;
        Command* buttonB_;
};

void InputHandler::handleInput() {
    if (isPressed(BUTTON_X)) buttonX_->execute();
    else if (isPressed(BUTTON_Y)) buttonY_->execute();
    else if (isPressed(BUTTON_A)) buttonA_->execute();
    else if (isPressed(BUTTON_B)) buttonB_->execute();
}

/* This is very basic, and has a lot of assumed coupling baked in. That assumed coupling limits the usefulness of those command, i.e. the only thing the JumpCommand can make jump is the player. Instead of calling functions that find the commanded object themselves, we’ll pass in the object that we want to order around:
*/

class Command {
    public:
        virtual ~Command() {}
        virtual void execute(GameActor& actor) = 0;
        // GameActor is the game object that represents a character in the game world.
        // We pass it in to execute() so that the derived command can invoke methods on an actor of our choice
};

class JumpCommand : public Command {
    // Now we can use this one class to make any character in the game hop around.
    public:
        virtual void execute(GameActor& actor) { actor.jump(); }
};

Command* InputHandler::handleInput() {
    // Change handleInput() so that it returns commands
    // It can’t execute the command immediately since it doesn’t know what actor to pass in.
    // Here we take advantage of the fact that the command is a reified call — we can delay when the call is executed.
    if (isPressed(BUTTON_X)) return buttonX_;
    if (isPressed(BUTTON_Y)) return buttonY_;
    if (isPressed(BUTTON_A)) return buttonA_;
    if (isPressed(BUTTON_B)) return buttonB_;

    // Nothing pressed, so do nothing.
    return NULL;
}

// Some code that takes that command and runs it on the actor representing the player
Command* command = inputHandler.handleInput();
if (command) { command->execute(actor); }

/* We can now let the player control any actor in the game now by changing the actor we execute the commands on.
We can use this same command pattern as the interface between the AI engine and the actors; the AI code simply emits Command objects.
We can use different AI modules for different actors, or we can mix and match AI for different kinds of behavior. We can even bolt AI onto the player’s character. */

/* Without the Command pattern implementing undo is surprisingly hard, but with it’s a piece of cake. Let’s say 
We’re already using commands to abstract input handling, so every move the player makes is already encapsulated in them. For example, moving a unit may look like: */

class MoveUnitCommand : public Command {
    public:
        MoveUnitCommand(Unit* unit, int x, int y) 
            : unit_(unit), x_(x), y_(y)
        {}

        virtual void execute() { unit_->moveTo(x_, y_); }

    private:
        Unit* unit_;
        int x_, y_;
};

/* this is a little different from our previous commands. In the last example, we wanted to abstract the command from the actor that it modified. In this case, we specifically want to bind it to the unit being moved. An instance of this command isn’t a general “move something” operation that you could use in a bunch of contexts; it’s a specific concrete move in the game’s sequence of turns.
In some cases, like our first couple of examples, a command is a reusable object that represents a thing that can be done. Our earlier input handler held on to a single command object and called its execute() method anytime the right button was pressed. Here, the commands are more specific. They represent a thing that can be done at a specific point in time. This means that the input handling code will be creating an instance of this every time the player chooses a move. Something like: */

Command* handleInput() {
    Unit* unit = getSelectedUnit();

    if (isPressed(BUTTON_UP)) {
        // Move the unit up one.
        int destY = unit->y() - 1;
        return new MoveUnitCommand(unit, unit->x(), destY);
    }

    if (isPressed(BUTTON_DOWN)) {
        // Move the unit down one.
        int destY = unit->y() + 1;
        return new MoveUnitCommand(unit, unit->x(), destY);
    }

    // Other moves...

    return NULL;
}


class Command {
    // To make commands undoable, we define another operation each command class needs to implement:
    public:
        virtual ~Command() {}
        virtual void execute() = 0;
        virtual void undo() = 0;
        // An undo() method reverses the game state changed by the corresponding execute() method.
};



class MoveUnitCommand : public Command {
    // Here’s our previous move command with undo support.
    public:
        MoveUnitCommand(Unit* unit, int x, int y)
            : unit_(unit),
            xBefore_(0),
            yBefore_(0),
            x_(x),
            y_(y)
        {}

    virtual void execute() {
        // Remember the unit's position before the move
        // so we can restore it.
        xBefore_ = unit_->x();
        yBefore_ = unit_->y();

        unit_->moveTo(x_, y_);
    }

    virtual void undo() {
        unit_->moveTo(xBefore_, yBefore_);
    }

    private:
        Unit* unit_;
        int xBefore_, yBefore_;
        int x_, y_;
};


/* Supporting multiple levels of undo isn’t much harder. Instead of remembering the last command, we keep a list of commands and a reference to the “current” one. When the player executes a command, we append it to the list and point “current” at it. 
When the player chooses “Undo”, we undo the current command and move the current pointer back. When they choose “Redo”, we advance the pointer and then execute that command. If they choose a new command after undoing some, everything in the list after the current command is discarded. */

/*If you have the luxury of a language with real closures, use them! In some ways, the Command pattern is a way of emulating closures in languages that don’t have them. I say some ways here because building actual classes or structures for commands is still useful even in languages that have closures. If your command has multiple operations (like undoable commands), mapping that to a single function is awkward.
Defining an actual class with fields also helps readers easily tell what data the command contains. Closures are a wonderfully terse way of automatically wrapping up some state, but they can be so automatic that it’s hard to see what state they’re actually holding.

If we were building a game in JavaScript, we could create a move unit command just like this:

function makeMoveUnitCommand(unit, x, y) {
  // This function here is the command object:
  return function() {
    unit.moveTo(x, y);
  }
}

We could add support for undo as well using a pair of closures:

function makeMoveUnitCommand(unit, x, y) {
  var xBefore, yBefore;
  return {
    execute: function() {
      xBefore = unit.x();
      yBefore = unit.y();
      unit.moveTo(x, y);
    },
    undo: function() {
      unit.moveTo(xBefore, yBefore);
    }
  };
}
*/

/*You may end up with a lot of different command classes. It’s often helpful to define a concrete base class with a bunch of convenient high-level methods that the derived commands can compose to define their behavior. That turns the command’s main execute() method into the Subclass Sandbox pattern. */